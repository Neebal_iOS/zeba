//
//  ZebaLogger.h
//  CarthageDemo
//
//  Created by Debojyoti on 27/04/18.
//  Copyright © 2018 Zeba. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZebaLogger : NSObject

-(void) logLine;

@end
